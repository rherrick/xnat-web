package org.nrg.pipeline.utils;

public class PipelineConstants {

    public static final String ALL_DATA_TYPES="All Datatypes";
    public static final boolean DEFAULT_RUN_PIPELINE_IN_PROCESS = false;
    public static final boolean DEFAULT_RECORD_WORKFLOW_ENTRIES = true;

}